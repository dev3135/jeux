$(function() {

    /** Création objet */
    function User(name, canPlay = false){
        this.name = name;
        this.score = 0;
        this.total = 0;
        this.canPlay = canPlay;
        this.canAddTotal = false;
        this.gagne = 0;
    }
    
    let user1 = new User("Joueur 1", true);
    let user2 = new User("Joueur 2", false);  

    const winGame = 100;

    $( ".rolldices" ).on( "click", function() {
        let scoreDes = generateRandomResult();
        changeImageByNumber(scoreDes);
        majScoresJoueurs(scoreDes);
        activeBtn('.add_scores');
        majVisuelScores();
    });

    $( ".add_scores" ).on( "click", function() {
        ajoutPoints();
        desactiveBtn('.add_scores');
        majVisuelGlobal();
        afficheModalGagnant();
    });

    $(".new_partie").on("click",function(){
        nouvellePartie();
        majVisuelGlobal();
        activeBtn('.add_scores');
        activeBtn('.rolldices');
    });

    /**
     * Nouvelle partie, réinitialisation à zero.
     */
    function nouvellePartie(){
        user1.total = 0;
        user1.score = 0;
        user2.total = 0;
        user2.score = 0; 
    }

    /**
     * Ajout des points 
     * Vérifie qui peut jouer.
     */
    function ajoutPoints(){
        if(user1.canAddTotal == true){
            user1.total += user1.score;
            user1.score = 0;
            user1.canPlay = false;
        }
        else {
            user2.total += user2.score ;
            user2.score = 0;
            user2.canPlay = false;
            user1.canPlay = true;
        }
    }

    /**
     *  Score par joueur + droit de jouer
     */
    function majScoresJoueurs(scoreDes){
        if(user1.canPlay == true){
            user1.score += scoreDes;
            user1.canPlay = false;
            user1.canAddTotal = true;
        }
        else {
            user2.score += scoreDes;
            
            user2.canPlay = false;
            user1.canPlay = true;
            
            user2.canAddTotal = true;
            user1.canAddTotal = false;
        }
    }


    function majVisuelGlobal(){
        majVisuelScores();
        majVisuelTotal();
    }

    function majVisuelScores(){
        $(".score1").html(user1.score);
        $(".score2").html(user2.score);
    }

    function majVisuelTotal(){
        $(".total1").html(user1.total);
        $(".total2").html(user2.total);
    }

    /**
     * Affiche modal joueur gagnant. 
     * Flowbite
     */
    function afficheModalGagnant(){
        const targetEl = document.getElementById('modal-win');
        const modal = new Modal(targetEl);
        if(user1.total >= winGame){
            $('.gagnant').html('Bravo au joueur 1 !');
            modal.show();
            desactiveBtn('.rolldices');
        }
        else if(user2.total >= winGame){
            $('.gagnant').html('Bravo au joueur 2 !');
            modal.show();
            desactiveBtn('.rolldices');
        }
        $(".close-modal").on("click",function(){
            modal.hide();
        });
    }
     
    function activeBtn(selector){
        $(selector).removeClass("cursor-not-allowed").removeAttr('disabled');
    }

    function desactiveBtn(selector){
        $(selector).addClass('cursor-not-allowed').prop( "disabled", true );
    }

    /*
    * Genére un résultat de 0 à max
    */
    function generateRandomResult(max = 6){
        return Math.floor(Math.random() * max) + 1;
    }

    /**
     * Changement image
    */ 
    function changeImageByNumber(number){
        $('#des0').attr('src',`images/d${number}.png`);
    }

});
